# Logger

Logger compatible with meson build system.

# Building 

With meson/ninja:
```bash
git clone https://github.com/ServiceDog/Logger.git
cd Logger
meson <build dir>
ninja -C <build dir>
```

# Testing

```bash
git clone https://github.com/ServiceDog/Logger.git
cd Logger
meson <build dir>
ninja -C <build dir>
meson test -C <build dir> --list
meson test -C <build dir> "<test name>" -v # Verbose output
```

