/**
 * @file LoggerServer.h
 * @author ServiceDog
 * @brief Receives messages from LoggerClients, writes them locally 
 * @date 2019-01-05
 * 
 */

#pragma once
#include "LoggerClient.h"

const std::string collatedLogName = "all";

class LoggerServer : public LoggerClient {
public:
    LoggerServer(
        const std::string& ipaddr,
        uint32_t port
    );
    ~LoggerServer();

    std::string getCollatedLogPath(void) {
        return LOG_DIR + collatedLogName + LOG_EXT;
    }
    
private:
    LoggerServer(const LoggerServer&) = delete;
    LoggerServer(const LoggerServer&&) = delete;
    void operator=(const LoggerServer&) = delete;

    void loop(void);

    std::atomic<bool> m_alive;
    std::thread m_serverThread;
    std::fstream m_allOut;

    std::string m_ip;
    uint32_t m_port;
};