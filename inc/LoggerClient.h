/**
 * @file LoggerClient.h
 * @author ServiceDog
 * @brief Writes info & error messages locally
 * @date 2019-01-05
 */
 
#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <dirent.h>
#include <atomic>
#include <thread>
#include "zmq.hpp"

const std::string INFO_TAG = "INFO";
const std::string ERROR_TAG = "ERROR";
const std::string LOG_DIR = "/tmp/";
const std::string LOG_EXT = ".log";
const std::string NONE = "";

class LoggerClient {
public:
    LoggerClient(
        const std::string& clientName,
        const std::string& ip = NONE, 
        const uint32_t port = 0
    );

    ~LoggerClient();

    void info(const std::string& msg);
    void error(const std::string& msg);

    std::string getLogPath(void) { return LOG_DIR + m_clientName + LOG_EXT; }

protected:
    zmq::context_t m_ctx;
    bool connectToServer(const std::string& ip, const uint32_t port);

private:
    LoggerClient(const LoggerClient&) = delete;
    LoggerClient(const LoggerClient&&) = delete;
    void operator=(const LoggerClient&) = delete;

    bool setOutputFile(const std::string& path);
    void printMsg(const std::string& tag, const std::string& msg_);

    std::string m_clientName;
    std::fstream m_stream;
    zmq::socket_t m_publisher; 
};