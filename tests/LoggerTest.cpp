/**
 * @file LoggerTest.cpp
 * @author ServiceDog
 * @brief Tests LoggerServer/LoggerClient communcation
 * @date 2019-01-05
 */

#define CATCH_CONFIG_MAIN
#include "LoggerServer.h"
#include <assert.h>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <catch2/catch.hpp>

const std::string ipaddr = "127.0.0.1";
const uint32_t port = 8001;
const std::string test_str = "Test 1 2 3";

class TestServer : public LoggerServer {
public:
    TestServer() : LoggerServer(ipaddr, port) {}
};

class TestClient : public LoggerClient {
public:
    TestClient(
        const std::string& name = "TestClient",
        const std::string& ipaddr_ = NONE,
        const uint32_t port_ = 0) : LoggerClient(name, ipaddr_, port_) {}
};

TEST_CASE("ZMQ PUSH PULL works") {
    zmq::context_t ctx(1);
    
    zmq::socket_t puller(ctx, ZMQ_PULL);
    REQUIRE_NOTHROW(puller.bind("tcp://" + ipaddr + ":" + std::to_string(port)));
    
    zmq::socket_t pusher(ctx, ZMQ_PUSH);
    REQUIRE_NOTHROW(pusher.connect("tcp://" + ipaddr + ":" + std::to_string(port)));

    zmq::message_t msg(test_str.length());
    memcpy(msg.data(), test_str.c_str(), test_str.length());
    pusher.send(msg, ZMQ_NOBLOCK);

    // Erase message to make sure we receive a new one
    memset(msg.data(), 0, test_str.length());
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    int ret = puller.recv(&msg, ZMQ_NOBLOCK);
    REQUIRE(ret > 0);

    auto str = std::string(static_cast<char*>(msg.data()), msg.size());
    REQUIRE(str.compare(test_str) == 0);

    pusher.close();
    puller.close();
    ctx.close();
}

TEST_CASE_METHOD(TestClient, "TestClient unit tests", "[]") {
    // Test output
    info("Testing info.");
    error("Testing error");
    
    auto path = getLogPath();

    // Confirm file contains text
    std::ifstream in(path);
    std::string contents(
        (std::istreambuf_iterator<char>(in)),
        std::istreambuf_iterator<char>()
    );

    REQUIRE(contents.find(INFO_TAG) != std::string::npos);
    REQUIRE(contents.find(ERROR_TAG) != std::string::npos);
}

TEST_CASE_METHOD(TestServer, "LoggerServer receives message") {
    // Give time for server to start
    std::this_thread::sleep_for(std::chrono::milliseconds(100));        
    info(test_str);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    // Confirm file contains text
    std::ifstream in(getCollatedLogPath());
    std::string contents(
        (std::istreambuf_iterator<char>(in)),
        std::istreambuf_iterator<char>()
    );

    REQUIRE(contents.find(test_str) != std::string::npos);
}

TEST_CASE("Server and Client Test") {
    const std::string nameA = "oogie", nameB = "boogie";
    std::string logPath = "";

    // Own scope to avoid concurrent access of all.log
    TestServer server;
    logPath = server.getCollatedLogPath();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));    

    LoggerClient clientA(nameA, ipaddr, port);
    LoggerClient clientB(nameB, ipaddr, port);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));    

    std::ifstream in(logPath);
    std::string contents(
        (std::istreambuf_iterator<char>(in)),
        std::istreambuf_iterator<char>()
    );
    
    REQUIRE(contents.find(nameA) != std::string::npos);
    REQUIRE(contents.find(nameB) != std::string::npos);
};