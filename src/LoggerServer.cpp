/**
 * @file LoggerServer.cpp
 * @author ServiceDog
 * @brief Receives messages from LoggerClients, writes them locally 
 * @date 2019-01-05
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include "LoggerServer.h"

/**
 * @brief Construct a new Logger Server:: Logger Server object
 * 
 * @param ipaddr IP address of the server
 * @param subPort The port for clients to connect to
 */
LoggerServer::LoggerServer(
    const std::string& ipaddr, 
    const uint32_t port
) : LoggerClient("LoggerServer"),
    m_ip(ipaddr),
    m_port(port) 
{
    m_serverThread = std::thread([&](){ loop(); });
}

/**
 * @brief Destroy the Logger Server:: Logger Server object
 * 
 */
LoggerServer::~LoggerServer() {
    info("Server exiting.");

    m_alive = false;

    if (m_serverThread.joinable()) {
        m_serverThread.join();
    }

    m_allOut.close();
}

/**
 * @brief Receive loop for the LoggerServer
 * 
 */
void LoggerServer::loop(void) {
    zmq::socket_t socket(m_ctx, ZMQ_PULL);
    
    // Bind server socket
    try {
        socket.bind("tcp://" + m_ip + ":" + std::to_string(m_port));
        connectToServer(m_ip, m_port);
    } catch (...) {
        error("Unable to init server.");
        return;
    }

    // Remove log file if already exists
    auto path = getCollatedLogPath();
    remove(path.c_str());
    
    // If stream already exists from this file, catch error
    try {
        m_allOut.open(
            path.c_str(), 
            std::fstream::out | std::fstream::app
        );

        info("Opened file stream: " + path);
    } catch (...) {
        error("Failed to open stream.");
        throw;
    }

    info("Started server.");
    m_alive = true;
    zmq::message_t msg;
    while (m_alive) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        if (socket.recv(&msg, ZMQ_NOBLOCK)) {
            auto str = std::string(static_cast<char*>(msg.data()), msg.size());
            m_allOut << str << std::endl;
        }
    }
    socket.close();
}