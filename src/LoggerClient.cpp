/**
 * @file LoggerClient.cpp
 * @author ServiceDog
 * @brief Writes info & error messages locally
 * @date 2019-01-05
 */
 
#include "LoggerClient.h"

/**
 * @brief Construct a new Client and connect to a Server
 * 
 * @param ip The address of the server
 * @param port The port of the server
 * @param clientName The name of this client
 */
LoggerClient::LoggerClient(
    const std::string& clientName,
    const std::string& ip, 
    const uint32_t port
) : m_ctx(1),
    m_clientName(clientName),
    m_publisher(m_ctx, ZMQ_PUSH) {

    std::string logPath = LOG_DIR + clientName + LOG_EXT;

    if (ip.compare(NONE) != 0) {
        if (connectToServer(ip, port)) {
            info("Connected to the server.");
        }
    }

    // Attempt to set output file
    if (!setOutputFile(logPath)) {
        throw;
    }

    info("Opened file stream: " + logPath);
}

/**
 * @brief Destroy the Logger Client:: Logger Client object
 * 
 */
LoggerClient::~LoggerClient() {
    m_stream.close();
    m_publisher.close();
    m_ctx.close();
}

//---------------------------------------------------------------------------//
// Print Functions
//---------------------------------------------------------------------------//
/**
 * @brief Prints a message to stdout (and a file, if stream is open)
 * 
 * @param tag A brief descriptor indicating the type of message
 * @param msg_ The message
 */
void LoggerClient::printMsg(const std::string& tag, const std::string& msg_) {
    std::ostringstream ss;

    // Add timestamp if enabled
    auto now = std::chrono::system_clock::now();
    auto now_c = std::chrono::system_clock::to_time_t(
        now - std::chrono::hours(24)
    );
    
    ss << "[" << std::put_time(std::localtime(&now_c), "%T") << "]";

    // Add tag
    ss << "(" << m_clientName << ")";
    ss << "[" << tag << "]";
    
    // Add Message
    ss << " " << msg_;
    
    // Create full string
    std::string full = ss.str();

    // Print to stdout
    std::cout << full << std::endl;

    // Write to file if open
    if (m_stream.is_open()) {
        m_stream << full << std::endl;
    }

    zmq::message_t reply(full.length());
    memcpy(reply.data(), full.c_str(), full.length());
    m_publisher.send(reply, ZMQ_NOBLOCK);
}

/**
 * @brief Prints a message with the "INFO" tag
 * 
 * @param msg_ Message to write
 */
void LoggerClient::info(const std::string& msg_) {
    printMsg(INFO_TAG, msg_);
}

/**
 * @brief Prints a message with the "ERROR" tag
 * 
 * @param msg_ Message to write
 */
void LoggerClient::error(const std::string& msg_) {
    printMsg(ERROR_TAG, msg_);
}

/**
 * @brief Connect to LoggerServer publishing socket
 * 
 * @param ip IP address of LoggerServer
 * @param port Port of LoggerServer
 * @return true If successful
 */
bool LoggerClient::connectToServer(const std::string& ip, const uint32_t port) {
    try {
        m_publisher.connect("tcp://" + ip + ":" + std::to_string(port));
    } catch (...) {
        return false;
    };

    return true;
}

/**
 * @brief Sets the output file where log statements are stored
 * 
 * @param path The path of the output file
 * @return true If successful
 */
bool LoggerClient::setOutputFile(const std::string& path) {
    // Remove log file if already exists
    remove(path.c_str());
    
    // If stream already exists from this file, catch error
    try {
        m_stream.open(
            path.c_str(), 
            std::fstream::out | std::fstream::app
        );
    } catch (...) {
        return false;
    }

    return true;
}